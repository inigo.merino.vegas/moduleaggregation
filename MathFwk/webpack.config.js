const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
    entry: {
        app: './src/index.ts'
    },

    output: {
        filename: 'index.js',
        path: path.resolve(__dirname, 'dist'),
        libraryTarget: 'commonjs2'
    },

    module: {
        rules: [{
            test: /\.(tsx|ts)?$/,
            use: 'ts-loader',
            exclude: /node_modules/
        }],
    },

    plugins: [
        new CleanWebpackPlugin(['dist'])
    ],

    resolve: {
        extensions: ['.tsx', '.ts', '.js']
    },

    devtool: 'inline-source-map'
};