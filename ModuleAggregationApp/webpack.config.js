const path = require('path');
const pkgjson = require('./package.json');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
    entry: {
        app: './src/index.ts'
    },

    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'dist')
    },

    module: {
        rules: [{
            test: /\.(tsx|ts)?$/,
            use: 'ts-loader',
            exclude: /node_modules/
        }],
    },

    plugins: [
        new CleanWebpackPlugin(['dist']),
        new HtmlWebpackPlugin({
            title: pkgjson.name,
            template: './src/index.html'
        })
    ],

    resolve: {
        extensions: ['.tsx', '.ts', '.js']
    },

    devtool: 'inline-source-map',

    devServer: {
        contentBase: './dist'
    }
};