import { Restador } from "mathfwk_inigo";
import { Sumador } from "mathfwk_inigo";

export class Main {

  public static main() {
    const restador: Restador = new Restador();

    const myElement: HTMLElement = document.getElementById("content")!;
    myElement.innerHTML = "HOLA MUNDO " + Sumador.suma(5, 3) + " - " + restador.resta(17, 3);
  }

}

Main.main();
